package main

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
	"log"
	"net/http"
)

type Product struct {
	Id    string `json:"id"`
	Name  string `json:"name"`
	Qty   string `json:"qty"`
	Price string `json:"price"`
}

func main() {
	client := redis.NewClient(&redis.Options{
		Addr:     "redis-service:6379",
		Password: "",
		DB:       0,
	})
	ctx := context.Background()

	topic := client.Subscribe(ctx, "pubsub-test")
	channel := topic.Channel()
	for msg := range channel {
		fmt.Println(msg.Payload)
	}

	//http.HandleFunc("/", subscribeHandler)
	log.Fatal(http.ListenAndServe(":9002", nil))
}

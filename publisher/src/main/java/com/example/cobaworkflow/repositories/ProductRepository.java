package com.example.cobaworkflow.repositories;

import com.example.cobaworkflow.dto.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product,String> {

}

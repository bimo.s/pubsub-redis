package com.example.cobaworkflow.dto;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

@Data
@RedisHash(value = "product")
public class Product {
    @Id
    @Indexed
    private String id;
    private String name;
    private int qty;
    private int price;
}

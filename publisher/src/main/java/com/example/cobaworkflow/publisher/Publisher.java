package com.example.cobaworkflow.publisher;

import com.example.cobaworkflow.dto.Product;
import com.example.cobaworkflow.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
public class Publisher {
    @Autowired
    private RedisTemplate template;
    @Autowired
    private ChannelTopic channelTopic;
    @Autowired
    private ProductRepository productRepository;
    @PostMapping("/publish")
    public String publish(@RequestBody Product product){
        template.convertAndSend(channelTopic.getTopic(), product.toString());
        return "event Publish";
    }

    @PostMapping("/product")
    public ResponseEntity<Product> add(@RequestBody Product product){
        Product save = productRepository.save(product);
        return new ResponseEntity<>(save, HttpStatus.CREATED);
    }

    @GetMapping("/product")
    public ResponseEntity<List<Product>> get() {
        List<Product> product = new ArrayList<>();
        productRepository.findAll().forEach(product::add);
        return new ResponseEntity<>(product, HttpStatus.OK);
    }

    @PutMapping("/product/{id}")
    public ResponseEntity<Product> update(@PathVariable(name = "id") String id, @RequestBody Product product) {
        Optional<Product> std = productRepository.findById(id);
        if (std.isPresent()) {
            Product findProduct = std.get();
            findProduct.setName(product.getName());
            findProduct.setPrice(product.getPrice());
            findProduct.setQty(product.getQty());
            Product updatedStudent = productRepository.save(findProduct);
            return new ResponseEntity<>(updatedStudent, HttpStatus.CREATED);
        }
        return null;
    }

    @DeleteMapping("/product/{id}")
    public ResponseEntity<String> delete(@PathVariable(name = "id") String id) {
        productRepository.deleteById(id);
        return new ResponseEntity<>("id: " + id + " deleted successfully", HttpStatus.OK);
    }
}
